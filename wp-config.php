<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'test' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '&VE0Haxs$7NnfJjs):T}I84z0o)HVB3W[3Re/9z$$I?s~U=LsP2h^sZ=aXko3CZ5' );
define( 'SECURE_AUTH_KEY',  '0R+T1hx^(&O+, dBuV/.M#GiZSc,$7/mX?YbC]Sw%*/5%)6UQ9Xd@rFaI6;SqVp]' );
define( 'LOGGED_IN_KEY',    '4QIl_U>K vW@Yj|8=4z3ckK[lLR/L 3SR,cyL)HC9M,_yS #)Bo]K7D0|$Rg)Ql*' );
define( 'NONCE_KEY',        ']!UNedH,O1bHUWPBk<T:?tV:?5[&Z!{Cn#a;8)dW;2sZ  FB%4hy@tMn{ )dj@(i' );
define( 'AUTH_SALT',        'aQc{P;KN`IHhCj1&h Ka*[gQ;p&)1.wVN?Qa8^>089W=@EmXj^_qhLG}a8IUW~Cr' );
define( 'SECURE_AUTH_SALT', 'uOhM!@B=`J%N,#Cmh;U9NMr?]uMlM-LQq<;$*DBt>kxJ8ZT0.A}m]cuVuj7Vyi1G' );
define( 'LOGGED_IN_SALT',   ',X`9(ag *&.Eu;aA9P^=5}9%<Iqa)Y7.}91afZwtOgNE8}TAc/Lj=:v]@]xJqtd!' );
define( 'NONCE_SALT',       '5kp_+& z#;m39Z9e%Ni[T0CmV5vi7D`lQXg;_45[S} F9!H2WM-:uUDl82H8q[!a' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
