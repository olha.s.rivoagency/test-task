<?php
/**
 * Plugin Name: RV Banks
 * Description:       Adding the banks and watch info
 * Version:           0.1
 * Requires at least: 5.2
 * Requires PHP:      5.6
 * Author:            Developer
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       rv-banks
 */

/*
* The code to register a WordPress Custom Post Type (CPT) 'rv_bank'
* 
*/
$plugindir = dirname( __FILE__ );


add_action('wp_enqueue_scripts', 'rv_banks_enqueue' );
function rv_banks_enqueue(){       

	wp_register_script('rv_jQ', 'https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js');
	wp_enqueue_script('rv_jQ');

    // JS
    wp_register_script('rv_bootstrap_js', plugins_url ('/assets/bootstrap/bootstrap.min.js', __FILE__ ));
	wp_enqueue_script('rv_bootstrap_js');

	
	// graph visualisation d3.js
	wp_register_script('rv_charts_js', 'https://www.gstatic.com/charts/loader.js');
	wp_enqueue_script('rv_charts_js');
	

	wp_register_script('rv_scripts_js',plugins_url ('/assets/js/rv_script.js', __FILE__ ));
	wp_enqueue_script('rv_scripts_js');


    // CSS
    wp_register_style('rv_bootstrap_css', plugins_url ('/assets/bootstrap/bootstrap.min.css', __FILE__ ));
	wp_enqueue_style('rv_bootstrap_css');
}




add_action( 'init', 'rv_banks_cpt' );
/**
 * Register a public CPT \
 */
function rv_banks_cpt() {

	// Post type should be prefixed, singular, and no more than 20 characters.
	register_post_type( 'rv_bank', array(
		// Label should be plural and L10n ready.
		'label'       => __( 'Banks', 'rv_bank' ),
		'public'      => true,
		'has_archive' => true,
		'rewrite'     => array(
			// Slug should be plural and L10n ready.
			'slug'        => _x( 'banks', 'CPT permalink slug', 'rv_bank' ),
			'with_front'  => false,
		),
		
		// Add support for the new block based editor (Gutenberg) by exposing this CPT via the REST API.
		'show_in_rest' => true,

		/**
		 * 'title', 'editor', 'thumbnail' 'author', 'excerpt','custom-fields',
		 * 'page-attributes' (menu order),'revisions' (will store revisions),
		 * 'trackbacks', 'comments', 'post-formats',
		 */
		'supports'    => array( 'title', 'editor', 'custom-fields' ),

		// Url to icon or choose from built-in https://developer.wordpress.org/resource/dashicons/.
		'menu_icon'   => 'dashicons-feedback',
	) );

}

/**
 * Custom template for single page
 */

add_filter( 'single_template', 'load_my_custom_template');
function load_my_custom_template( $template ) {
	$plugindir = dirname( __FILE__ );

	if ( is_singular( 'rv_bank' ) ) {
		$template = $plugindir . '/templates/single-rv_bank.php';
	}
	
	return $template;
}

add_action('wp_ajax_get_exchange'       , 'rv_get_exchange');
add_action('wp_ajax_get_exchange', 'rv_get_exchange');

function rv_get_exchange(){
	
    wp_die();
}


/**
 * Activate the plugin.
 */
function rv_banks_activate() { 
    // Trigger our function that registers the custom post type plugin.
    rv_banks_cpt(); 
    // Clear the permalinks after the post type has been registered.
    flush_rewrite_rules(); 
}
register_activation_hook( __FILE__, 'rv_banks_activate' );

/**
 * Deactivation hook.
 */
function rv_banks_deactivate() {
    // Unregister the post type, so the rules are no longer in memory.
    unregister_post_type( 'rv_bank' );
    // Clear the permalinks to remove our post type's rules from the database.
    flush_rewrite_rules();
}
register_deactivation_hook( __FILE__, 'rv_banks_deactivate' );?>

