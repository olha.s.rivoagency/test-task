let api_id = 'a22e6f2ff45a40c4ba78051982234c96';
jQuery.get('https://openexchangerates.org/api/latest.json', {app_id: api_id}, function(data) {
    let item_GBP = data.rates.GBP;
    let usd = data.rates.USD;
    let cad = data.rates.CAD;
    let eur = data.rates.EUR;
    google.charts.load('current', {'packages':['table']});
    google.charts.setOnLoadCallback(drawTable);

    function drawTable() {
      var data = new google.visualization.DataTable();
      data.addColumn('string', '');
      data.addColumn('number', 'USD');
      data.addColumn('number', 'GBP');
      data.addColumn('number', 'CAD');
      data.addColumn('number', 'EUR');
      data.addRows([
        ['USD',  {v: usd}, {v: item_GBP}, {v: cad}, {v: eur}],
        
      ]);

      var table = new google.visualization.Table(document.getElementById('current_chart_div'));

      table.draw(data, {showRowNumber: true, width: '100%', height: '100%'});
    }
                            
});
   