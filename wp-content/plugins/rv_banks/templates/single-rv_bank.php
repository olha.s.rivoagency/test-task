<?php
/**
 * The template for displaying single posts of custom post type rv-banks
 *
 * @package rv_banks
 *
 */
get_header();
?>

<main id="site-content" role="main">

	<?php

	if ( have_posts() ) {

		while ( have_posts() ) :
			the_post();?>

		
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                <header class="entry-header alignwide">
                    <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
                </header>

                <div class="entry-content">
                    <?php
                    the_content();              
                    ?>

                    
                    <!-- filelds -->
                    <?php 
                    $rv_bank_capital = get_field('rv_bank_capitalization');
                    $rv_bank_rating = get_field('rv_bank_rating');
                    $rv_bank_link = get_field('rv_bank_link');
                    ?>

                  <div class="row justify-content-between">

                    <?php if($rv_bank_capital){?>
                        <div class="col-5 m-1 bg-light">
                            <p class="lead">The Bank Capitalization</p>
                            <span class="label label-info"><?php echo $rv_bank_capital;?></span>
                        </div>
                    <?php }?>

                    <?php if($rv_bank_rating){?>
                        <div class="col-5 m-1 bg-light">
                            <p class="lead">The Bank Rating</p>
                            <div><?php echo $rv_bank_rating;?></div>
                        </div>
                    <?php }?>

                    <?php if($rv_bank_link){?>
                        <div class="col-12 mt-4 ">
                            <p class="lead">The Bank Web-Site:
                                <a href="<?php echo $rv_bank_link;?>"><?php the_title();?></a> 
                            </p>
                        </div>
                    <?php }?>
                  
                  </div>  

                <?php 
                $images = get_field('rv_bank_gallary');
                if( $images ): ?>
                    <div class="row align-items-center" id="gallery" data-toggle="modal" data-target="#exampleModal">
                    <?php foreach( $images as $image ): ?>
                        <div class="col-12 col-sm-6 col-lg-4">
                            <img class="w-100" src="<?php echo esc_url($image['sizes']['large']); ?>" data-target="#carouselExample" data-slide-to="0">
                        </div>
                
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>

                    <div class="container rv_bank_info"><!--rv_bank_container -->

                        <div class="row mt-5">
                            <div class="col-12">
                                <h3> Current Exchange Rate</h3>
                          
                                <div class="p-2 bg-light mt-3">
                                    <div id="current_chart_div"></div>
                                </div>
                            </div>
                       
                            <div class="col-12 mt-5" >
                                <button id="btn_rv_history" class="col-12 btn btn-secondary">View The History </button>
                                
                                <div class="p-2 bg-light mt-3">
                                <label >History for last week: </label>
                                <div id="chart_div"></div>
                                </div>
                            </div>

                        </div>
                    </div><!--rv_bank_container -->

                </div><!-- .entry-content -->

                <script type="text/javascript">
     
                  


                </script>
  
                </article><!-- #post-<?php the_ID(); ?> -->

		<?php endwhile;
	}

	?>

</main><!-- #site-content -->


<script>
jQuery(function($){
    let api_id = 'a22e6f2ff45a40c4ba78051982234c96';
                    let date = '2021-01-01';
                    let date2 = '2020-01-01';
                    let date3 = '2019-01-01';

                        jQuery.get('https://openexchangerates.org/api/historical/' + date + '.json', {app_id: api_id}, function(data) {
                         
                            console.log("On " + date + ", 1 US Dollar was worth " + data.rates.GBP + " GBP");
                    });

                    jQuery.get('https://openexchangerates.org/api/historical/' + date2 + '.json', {app_id: api_id}, function(data) {
                        console.log("On " + date + ", 1 US Dollar was worth " + data.rates.GBP + " GBP");
                    });

                    jQuery.get('https://openexchangerates.org/api/historical/' + date3 + '.json', {app_id: api_id}, function(data) {
                        console.log("On " + date + ", 1 US Dollar was worth " + data.rates.GBP + " GBP");
                    });

	$('#btn_rv_history').click(function(){
		$.ajax({
			url: '<?php echo admin_url("admin-ajax.php") ?>',
			type: 'GET',
            data: {
                action : 'get_exchange'
            },
			beforeSend: function( xhr ) {
				$('#btn_rv_history').text('Loading, wait...');	
			},
			success: function( data ) {
				$('#btn_rv_history').text('update');	
                google.charts.load('current', {'packages':['corechart']});
                google.charts.setOnLoadCallback(drawChart);
                function drawChart() {
                             var data = new google.visualization.DataTable();
                             data.addColumn('number', 'x');
                             data.addColumn('number', 'values');
                             data.addColumn({id:'i0', type:'number', role:'interval'});
                             data.addColumn({id:'i1', type:'number', role:'interval'});
                             data.addColumn({id:'i2', type:'number', role:'interval'});
                
                            // todo: implement values of exchange data
                           
                             data.addRows([
                                [1, 100, 90, 110, 85],
                                [2, 120, 95, 130, 90],
                                [3, 130, 105, 140, 100]
                            ]);
                              
                                 var options_lines = {
                            title: 'Line intervals, default',
                            curveType: 'function',
                            lineWidth: 4,
                            intervals: { 'style':'line' },
                            legend: 'none'
                        };

                        var options_lines = {
                            title: 'Line intervals, default',
                            curveType: 'function',
                            lineWidth: 4,
                            intervals: { 'style':'line' },
                            legend: 'none'
                        };
                  
                        var chart_lines = new google.visualization.LineChart(document.getElementById('chart_div'));
                        chart_lines.draw(data, options_lines);
                            }
			}
		});

	});
});
</script>
<?php get_footer(); ?>